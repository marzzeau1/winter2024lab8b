import java.util.Random;

public class Board{
	private Tile[][] grid;
	private final int SIZE = 5;
	Random rng = new Random();
	
	public Board(){
		grid = new Tile[SIZE][SIZE];
		for(int i = 0; i < grid.length; i++){
			int randIndex = rng.nextInt(grid[i].length);
			for(int j = 0; j < grid[i].length; j++){
				if(j == randIndex){
					grid[i][j] = Tile.HIDDEN_WALL;
				}else{
					grid[i][j] = Tile.BLANK;
				}
			}
		}
	}
	public String toString(){
		String output = "";
		for(int i = 0; i < grid.length; i++){
			for(int j = 0; j < grid[i].length; j++){
				output += this.grid[i][j].getName() + " ";
			}
			output += "\n";
		}
		return output;
	}
	public int placeToken(int row, int col){
		if(row >= 0 && row < 5 && col > 5 && col >= 0){
			return -2;
		}else if(this.grid[row-1][col-1].equals(Tile.CASTLE) || this.grid[row-1][col-1].equals(Tile.WALL)){
			return -1;
		}else if(this.grid[row-1][col-1].equals(Tile.HIDDEN_WALL)){
			this.grid[row-1][col-1] = Tile.WALL;
			return 1;
		}else{
		this.grid[row-1][col-1] = Tile.CASTLE;
		return 0;
		}
	}
	
}