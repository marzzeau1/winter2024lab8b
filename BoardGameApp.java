import java.util.Scanner;

public class BoardGameApp{
	public static void main(String[] args){
		Scanner reader = new Scanner(System.in);
		Board grid = new Board();
		System.out.println("Welcome to castle game thing!!!!");
		System.out.println(grid);
		int numCastles = 7;
		int turns = 0;
		int turnsLeft = 8;
		
		while(numCastles > 0 && turns < 9){
			System.out.println("You have " + (turnsLeft - turns) + " turns left. You have " + numCastles + " castles left to place.");
			System.out.println("Please input the row you'd like to input");
				int row = Integer.parseInt(reader.nextLine());
			System.out.println("Please input the column you'd like to input");
				int col = Integer.parseInt(reader.nextLine());
			int placedToken = grid.placeToken(row, col);
			
			System.out.println(grid);
		
			while(placedToken < 0){
				System.out.println("Please re-input the row you'd like to input");
					row = Integer.parseInt(reader.nextLine());
				System.out.println("Please re-input the column you'd like to input");
					col = Integer.parseInt(reader.nextLine());
				placedToken = grid.placeToken(row, col);
			}
			
			if(placedToken == 1){
				System.out.println("There was a wall there, GET GOT!!!! -1 turn :(");
				turns += 1;
			}else if(placedToken == 0){
				System.out.println("YOU GOT A CASTLE DOWN!!! LET'S GOOOOO!!!!!!!!! +1 castle, +1 turn :)");
				numCastles -= 1;
			}
		}
		if(numCastles == 0){
			System.out.println("YOU WIN!!!!!!!!!!!!!!!!! YOU GOT ALL THE CASTLES DOWN!!!!!!!");
		}else{
			System.out.println("YOU LOSE!!!!!!!");
		}
	}
}